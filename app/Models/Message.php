<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use App\Traits\HandleUploadedImage;

class Message extends Model
{
    use HandleUploadedImage, SoftDeletes;

    protected static $imagePath = 'images/messages';

    protected $fillable = [
        'name', 'title', 'body', 'password', 'image'
    ];

    public static function boot()
    {
        parent::boot();

        static::updated(function ($message) {
            if ($message->wasChanged('image')) {
                self::removeImage($message->getOriginal('image'));
            }
        });

        static::deleted(function ($message) {
            self::removeImage($message->image);
        });
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function setPasswordAttribute($value)
    {
        if ($value !== null && $value !== '') {
            $this->attributes['password'] = Hash::make($value);
        }
    }

    public function setImageAttribute($image)
    {
        $this->attributes['image'] = ltrim($image, self::$imagePath . '/');
    }

    public function getFullImageAttribute()
    {
        return 'storage/' . self::getImagePath() . '/' . $this->image;
    }

    public function getThumbnailImageAttribute()
    {
        return 'storage/' . self::getThumbnailPath() . '/' . $this->image;
    }
}
