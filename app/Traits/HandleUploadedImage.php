<?php

namespace App\Traits;

use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

/**
 * This is trait to handle uploaded image
 */
trait HandleUploadedImage
{
    public static function storeImage($image, string $disk = 'public')
    {
        return $image->store(self::getImagePath(), $disk);
    }

    public static function createThumbnail($image, int $size, string $disk = 'public')
    {
        $thumbnailPath = $image->store(self::getThumbnailPath(), $disk);

        $thumbnail = Image::make(public_path("storage/{$thumbnailPath}"))->fit($size);

        $thumbnail->save();
    }
    
    public static function removeImage($image, string $disk = 'public')
    {
        Storage::disk($disk)->delete(self::getImagePath() . '/' . $image);
        Storage::disk($disk)->delete(self::getThumbnailPath() . '/' . $image);
    }

    public static function getImagePath()
    {
        return self::$imagePath ?? 'images';
    }

    public static function getThumbnailPath()
    {
        return self::$thumbnailPath ?? self::getImagePath() . '/thumbs';
    }
}


