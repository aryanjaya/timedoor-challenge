<?php

namespace App\Traits;

/**
 * This is trait to handle uploaded file
 */
trait HandleUploadedFile
{
    protected function storeFile($image, string $path, string $disk = 'public')
    {
        $imagePath = $image->store($path, $disk);

        return ltrim($imagePath, "{$path}/");
    }
}


