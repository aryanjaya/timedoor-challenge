<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Access\Response;
use Illuminate\Auth\Access\HandlesAuthorization;

class MessagePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function modify(?User $user, $message, $password)
    {
        if (is_null($user)) {
            if ($message->user_id) {
                $this->deny('You cannot delete or edit other people\'s messages.');
            }

            if (! $message->password) {
                $this->deny('This message cannot be delete or edit because it has no password.');
            }
            
            if (! Hash::check($password, $message->password)) {
                $this->deny('wrong');
            }
        } elseif ($user->id !== $message->user_id) {
            $this->deny('You cannot delete or edit other people\'s messages.');
        }

        return true;
    }
}
