<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class MessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'     => ['nullable', 'string', 'between:3,16'],
            'title'    => ['required', 'string', 'between:10,32'],
            'body'     => ['required', 'string', 'between:10,200'],
            'password' => ['nullable', 'numeric', 'digits:4']
        ];
        
        if (! $this->has('delete')) {
            $rules['image'] = ['nullable', 'image', 'max:1024'];
        }

        return $rules;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        if ($this->routeIs('message.update')) {
            $this->errorBag = 'edit';
        }
    }
}
