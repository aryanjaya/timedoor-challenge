<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class GuestOrVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if (Auth::user()->is_admin) {
                return redirect()->route('dashboard.index');
            }

            if (!Auth::user()->hasVerifiedEmail()) {
                Auth::logout();
                
                return redirect()->route('login')->with('denied', true);
            }
        }

        return $next($request);
    }
}
