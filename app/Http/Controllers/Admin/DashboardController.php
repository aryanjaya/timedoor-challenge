<?php

namespace App\Http\Controllers\Admin;

use App\Models\Message;
use Illuminate\Http\Request;
use App\Traits\HandleUploadedImage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class DashboardController extends Controller
{
    use HandleUploadedImage;

    public function index(Request $request)
    {
        $messages = $this->searchMessage($request)->paginate(10);

        $messages->appends($request->only('title', 'body', 'image_filter', 'status'));

        return view('admin.index', [
            'messages' => $messages,
            'page' => 'home'
        ]);
    }

    public function multiDelete(Request $request)
    {
        if (is_array($request->id)) {
            $messages = Message::whereIn('id', $request->id)->get();

            Message::whereIn('id', $request->id)->update(['image' => null]);
            Message::whereIn('id', $request->id)->delete();

            foreach ($messages as $message) {
                Message::removeImage($message->image);
            }
        }

        return back()->with('notif', 'deleted');
    }

    public function recover($id)
    {
        Message::where('id', $id)->restore();

        return back()->with('notif', 'recovered');
    }

    public function delete($id)
    {
        $message = Message::find($id);
        
        $message->update(['image' => null]);
        $message->delete();

        return back()->with('notif', 'deleted');
    }

    public function deleteImage($id)
    {
        Message::find($id)->update(['image' => null]);

        return back()->with('notif', 'deleted');
    }

    protected function searchMessage(Request $request)
    {
        $condition = [
            ['title', 'like', "%{$request->title}%"],
            ['body', 'like', "%{$request->body}%"]
        ];

        if ($request->status === 'on') {
            $messages = Message::where($condition);
        } elseif ($request->status === 'delete') {
            $messages = Message::onlyTrashed()->where($condition);
        } else {
            $messages = Message::withTrashed()->where($condition);
        }

        if ($request->image_filter === 'with') {
            $messages = $messages->whereNotNull('image');
        } elseif ($request->image_filter === 'without') {
            $messages = $messages->whereNull('image');
        }

        return $messages->latest();
    }
}
