<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;
use App\Http\Requests\MessageRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class MessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('check.verified');
    }

    public function index()
    {
        $messages = Message::latest()->paginate(10);

        if (session('id') || old('id')) {
            $msg = Message::find(session('id') ?? old('id'));
        }

        return view('message.index', [
            'messages' => $messages,
            'msg'      => $msg ?? ''
        ]);
    }

    public function store(MessageRequest $request)
    {
        $input = $request->validated();

        if ($request->hasFile('image')) {
            $input['image'] = Message::storeImage($request->image);
            Message::createThumbnail($request->image, 250);
        }

        if (Auth::check()) {
            Auth::user()->messages()->create($input);
        } else {
            Message::create($input);
        }

        return redirect('/');
    }

    public function delete(Request $request)
    {
        if (! $message = Message::find($request->id)) {
            return back()->with(['modal' => 'fail']);
        }

        try {
            $this->authorize('modify', [$message, $request->password]);
        } catch (\Exception $e) {
            return $this->redirectUnauthorizeModify($e->getMessage(), $request->id, 'delete');
        }
        
        return back()->with([
            'id'       => $request->id,
            'password' => $request->password,
            'modal'    => 'delete'
        ]);
    }
    
    public function destroy(Request $request)
    {
        if (! $message = Message::find($request->id)) {
            return back()->with(['modal' => 'fail']);
        }

        try {
            $this->authorize('modify', [$message, $request->password]);
        } catch (\Exception $e) {
            return $this->redirectUnauthorizeModify($e->getMessage(), $request->id, 'delete');
        }
        
        $message->forceDelete();
        
        return redirect('/');
    }
    
    public function edit(Request $request)
    {
        if (! $message = Message::find($request->id)) {
            return back()->with(['modal' => 'fail']);
        }

        try {
            $this->authorize('modify', [$message, $request->password]);
        } catch (\Exception $e) {
            return $this->redirectUnauthorizeModify($e->getMessage(), $request->id, 'edit');
        }
        
        return back()->with([
            'id'       => $request->id,
            'password' => $request->password,
            'modal'    => 'edit'
        ]);
    }
    
    public function update(MessageRequest $request)
    {
        if (! $message = Message::find($request->id)) {
            return back()->with(['modal' => 'fail']);
        }

        try {
            $this->authorize('modify', [$message, $request->confirm_password]);
        } catch (\Exception $e) {
            return $this->redirectUnauthorizeModify($e->getMessage(), $request->id, 'edit');
        }
        
        $input = $request->validated();
        
        if ($request->has('delete')) {
            $input['image'] = null;
        } elseif ($request->hasFile('image')) {
            $input['image'] = Message::storeImage($request->image);
            Message::createThumbnail($request->image, 250);
        }
        
        $message->update($input);

        return redirect('/');
    }

    protected function redirectUnauthorizeModify($message, $id, string $action)
    {
        if ($message === 'wrong') {
            return back()->with([
                'modal'  => 'wrong',
                'action' => $action,
                'id'     => $id
            ]);
        }

        return back()->with([
            'modal'   => 'fail',
            'message' => $message
        ]);
    }

}
