<div class="modal fade" id="wrongModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">{{ ucfirst(session('action')) }} Data</h4>
      </div>
      <form id="form-wrong" method="POST" action="{{ route('message.' . session('action'), ['id' => $msg->id]) }}">
        @csrf
        <div class="modal-body pad-20">
          <div class="clearfix">
            <div class="pull-left">
              <h2 class="mb-5 text-green"><b>{{ $msg->title }}</b></h2>
            </div>
            <div class="pull-right text-right">
              <p class="text-lgray">
                {{ $msg->created_at->format('d-m-Y') }}<br/>
                <span class="small">{{ $msg->created_at->format('H:i') }}</span>
              </p>
            </div>
          </div>
            
            <h4 class="mb-20">
              {{ $msg->name ?? 'No Name' }} <span class="text-id">ID - {{ $msg->user_id }}</span>
            </h4>
            
            <p>{!! nl2br(e($msg->body)) !!}</p>
            
          <div class="img-box my-15">
            @if ($msg->image)
              <img class="img-responsive" style="max-height: 200px;" src="{{ asset($msg->thumbnail_image) }}" alt="image">
            @else
              <img class="img-responsive" style="max-height: 200px;" src="http://via.placeholder.com/1200x500" alt="image">
            @endif
          </div>

          <div class="form-group">
            <label>Password</label>
            <input type="password" name="password" class="form-control">
            <p class="small text-danger mt-5">Password doesn't match!!</p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button id="confirmBtn" type="submit" class="btn btn-danger">{{ ucfirst(session("action")) }}</button>
        </div>
      </form>
    </div>
  </div>
</div>