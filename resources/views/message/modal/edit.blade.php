<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Item</h4>
      </div>
      <form id="formUpdate" method="POST" action="{{ route('message.update', ['id' => $msg->id]) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <input type="hidden" name="id" value="{{ $msg->id }}">

        @guest
          <input type="hidden" name="confirm_password" value="{{ old('confirm_password') ?? session('password') }}">
        @endguest

        <div class="modal-body">
          <div class="form-group">
            <label>Name</label>
            <input type="text" name="name" class="form-control" value="{{ old('name') ?? $msg->name }}">
            @if ($errors->edit->has('name'))
              <p class="small text-danger mt-5">{{ $errors->edit->first('name') }}</p>
            @endif
          </div>

          <div class="form-group">
            <label>Title</label>
            <input type="text" class="form-control" name="title" value="{{ old('title') ?? $msg->title }}">
            @if ($errors->edit->has('title'))
              <p class="small text-danger mt-5">{{ $errors->edit->first('title') }}</p>
            @endif
          </div>

          <div class="form-group">
            <label>Body</label>
            <textarea rows="5" name="body" class="form-control">{{ old('body') ?? $msg->body }}</textarea>
            @if ($errors->edit->has('body'))
              <p class="small text-danger mt-5">{{ $errors->edit->first('body') }}</p>
            @endif
          </div>
          <div class="form-group row">
            <div class="col-md-4">
              @if ($msg->image)
                <img class="img-responsive" src="{{ asset($msg->thumbnail_image) }}" alt="image">
              @else
                <img class="img-responsive" src="http://via.placeholder.com/1200x500" alt="image">
              @endif
            </div>
            <div class="col-md-8 pl-0">
              <label>Choose image from your computer :</label>
              <div class="input-group">
                <input type="text" class="form-control upload-form" value="No file chosen" readonly>
                <span class="input-group-btn">
                  <span class="btn btn-default btn-file">
                    <i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" name="image" multiple>
                  </span>
                </span>
              </div>
              @if ($errors->edit->has('image'))
                <p class="small text-danger mt-5">{{ $errors->edit->first('image') }}</p>
              @endif
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="delete" value="delete">Delete image
                </label>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" id="updateBtn" data-id="{{ $msg->id }}">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>