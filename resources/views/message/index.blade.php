@extends('layouts.base')

@section('title', 'Timedoor Challenge - Level 9')

@section('css')
    
@endsection

@section('content')
  <div class="section">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3 bg-white p-30 box">
          <div class="text-center">
            <h1 class="text-green mb-30"><b>Level 9 Challenge</b></h1>
          </div>
          
          @include('message.form-create')

          <hr>
          @include('message.list')

          <div class="text-center mt-30">
            <nav>
              {{ $messages->links() }}
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>

  @if (session('modal') === 'delete')
    @include('message.modal.delete')
  @endif

  @if ($errors->hasBag('edit') || session('modal') === 'edit')
    @include('message.modal.edit')
  @endif

  @if (session('modal') === 'fail')
    @include('message.modal.fail')
  @endif

  @if (session('modal') === 'wrong')
    @include('message.modal.wrong')
  @endif
  
@endsection

@section('script')
    <script type="text/javascript" src="/js/custom.js"></script>

    <script type="text/javascript">
      @if (session('modal') === 'delete')
        $(document).ready(function () {
          $('#deleteModal').modal('show');
        });
      @endif

      @if ($errors->hasBag('edit') || session('modal') === 'edit')
        $(document).ready(function () {
          $('#editModal').modal('show');
        });
      @endif
      
      @if (session('modal') === 'fail')
        $(document).ready(function () {
          $('#failModal').modal('show');
        });
      @endif
        
      @if (session('modal') === 'wrong')
        $(document).ready(function () {
          $('#wrongModal').modal('show');
        });
      @endif
    </script>
@endsection