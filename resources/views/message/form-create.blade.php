<form action="" method="post" enctype="multipart/form-data">
  @csrf

  <div class="form-group">
    <label>Name</label>

    @if ($errors->hasBag('edit'))
      <input type="text" name="name" class="form-control" value="{{ auth()->user()->name ?? '' }}">
    @else
      <input type="text" name="name" class="form-control" value="{{ old('name') ?? auth()->user()->name ?? '' }}">
    @endif

    @if ($errors->has('name'))
      <p class="small text-danger mt-5">{{ $errors->first('name') }}</p>
    @endif
  </div>

  <div class="form-group">
    <label>Title</label>
    <input type="text" name="title" class="form-control" value="{{ $errors->hasBag('edit')? '': old('title') }}">
    @if ($errors->has('title'))
      <p class="small text-danger mt-5">{{ $errors->first('title') }}</p>
    @endif
  </div>

  <div class="form-group">
    <label>Body</label>
    <textarea rows="5" name="body" class="form-control">{{ $errors->hasBag('edit')? '': old('body') }}</textarea>
    @if ($errors->has('body'))
      <p class="small text-danger mt-5">{{ $errors->first('body') }}</p>
    @endif
  </div>

  <div class="form-group">
    <label>Choose image from your computer :</label>
    <div class="input-group">
      <input type="text" class="form-control upload-form" value="No file chosen" readonly>
      <span class="input-group-btn">
        <span class="btn btn-default btn-file">
          <i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" name="image" multiple>
        </span>
      </span>
    </div>
    @if ($errors->has('image'))
      <p class="small text-danger mt-5">{{ $errors->first('image') }}</p>
    @endif
  </div>

  @guest
    <div class="form-group">
      <label>Password</label>
      <input type="password" name="password" class="form-control">
      @if ($errors->has('password'))
        <p class="small text-danger mt-5">{{ $errors->first('password') }}</p>
      @endif
    </div>
  @endguest

  <div class="text-center mt-30 mb-30">
    <button type='submit' class="btn btn-primary">Submit</button>
  </div>
</form>