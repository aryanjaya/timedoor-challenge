@foreach ($messages as $message)
  <div class="post">
    <div class="clearfix">
      <div class="pull-left">
        <h2 class="mb-5 text-green"><b>{{ $message->title }}</b></h2>
      </div>
      <div class="pull-right text-right">
        <p class="text-lgray">
          {{ $message->created_at->format('d-m-Y') }}<br/>
          <span class="small">{{ $message->created_at->format('H:i') }}</span>
        </p>
      </div>
    </div>

    <h4 class="mb-20">
      {{ $message->name ?? 'No Name' }} <span class="text-id">ID - {{ $message->user_id }}</span>
    </h4>

    <p>{!! nl2br(e($message->body)) !!}</p>
    
    <div class="img-box my-15">
      @if ($message->image)
        <img class="img-responsive center-block" src="{{ asset($message->thumbnail_image) }}" alt="image">
      @else
        <img class="img-responsive center-block" src="http://via.placeholder.com/1200x500" alt="image">
      @endif
    </div>
    
    @guest
      @if (! $message->user_id)
        <form class="form-inline mt-50" method="POST">
          @csrf
          <input type="hidden" name="id" value="{{ $message->id }}">
          <div class="form-group mx-sm-3 mb-2">
            <label class="sr-only">Password</label>
            <input type="password" name="password" class="form-control">
          </div>
          <button type="submit" formaction="{{ route('message.edit') }}" class="btn btn-default mb-2">
            <i class="fa fa-pencil p-3"></i>
          </button>
          <button type="submit" formaction="{{ route('message.delete') }}" class="btn btn-danger mb-2">
            <i class="fa fa-trash p-3"></i>
          </button>
        </form>
      @endif
    @else
      @if ($message->user_id && $message->user_id === auth()->user()->id)
        <form class="form-inline mt-50"  method="POST">
          @csrf
          <input type="hidden" name="id" value="{{ $message->id }}">
          <button type="submit" formaction="{{ route('message.edit') }}" class="btn btn-default mb-2">
            <i class="fa fa-pencil p-3"></i>
          </button>
          <button type="submit" formaction="{{ route('message.delete') }}" class="btn btn-danger mb-2">
            <i class="fa fa-trash p-3"></i>
          </button>
        </form>
      @endif
    @endguest
  </div>

@endforeach