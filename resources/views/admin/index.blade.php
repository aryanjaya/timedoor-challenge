@extends('layouts.dashboard')

@section('title', 'Dashboard')

@section('css')
    
@endsection

@section('content')
  <section class="content">
    <div class="row">
      <div class="col-xs-12"><!-- /.col-xs-12 -->
        <div class="box box-success">
          <div class="box-header with-border">
            <h1 class="font-18 m-0">Timedoor Challenge - Level 9</h1>
          </div>
          <div class="box-body">
            
            @include('admin.search-form')

            @include('admin.search-result')
            
            <div class="text-center">
              <nav>
                {{ $messages->links() }}
              </nav>
            </div>
          </div>
        </div>
      </div><!-- /.col-xs-12 -->
    </div>
  </section>

  @if (session('notif'))
  <div class="alert alert-notif fade notification" tabindex="-1" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <strong>Info:</strong> The item was <strong>{{ session('notif') }}</strong> successfully
  </div>
  @endif

  <div class="modal fade" id="deleteMessageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <div class="text-center">
            <h4 class="modal-title" id="myModalLabel">Delete Message</h4>
          </div>
        </div>
        <div class="modal-body pad-20">
          <p>Are you sure want to delete this message?</p>
        </div>
        <div class="modal-footer">
          <form id="deleteMessageForm" action="" method="post">
            @csrf
            @method('DELETE')
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-danger">Delete</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="deleteImageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <div class="text-center">
            <h4 class="modal-title" id="myModalLabel">Delete Image</h4>
          </div>
        </div>
        <div class="modal-body pad-20">
          <p>Are you sure want to delete this Image?</p>
        </div>
        <div class="modal-footer">
          <form id="deleteImageForm" action="" method="post">
            @csrf
            @method('DELETE')
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-danger">Delete</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="deleteSelectedModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <div class="text-center">
            <h4 class="modal-title" id="myModalLabel">Delete Messages</h4>
          </div>
        </div>
        <div class="modal-body pad-20">
          <p>Are you sure want to delete this Messages?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" form="formResult" formaction="{{ route('dashboard.message.multi-delete') }}" class="btn btn-danger">Delete</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    $('#selectAll').click(function () {
      $('input:checkbox').not(this).prop('checked', this.checked);     
      $('#deleteAll').prop('disabled', !this.checked);
    });

    $('input:checkbox').not('#selectAll').change(function () {
      var numOfChecked  = $('input:checkbox:checked').not('#selectAll').length;
      var numOfCheckbox = $('input:checkbox').not('#selectAll').length;

      if (numOfChecked > 0) {  
        $('#deleteAll').prop('disabled', false);
      } else {
        $('#deleteAll').prop('disabled', true);
      }

      if (numOfChecked === numOfCheckbox) {
        $('#selectAll').prop('checked', true);
      } else {
        $('#selectAll').prop('checked', false);
      }
    });

    $('#deleteMessageModal').on('show.bs.modal', function (e) {
      var id  = $(e.relatedTarget).data('id');
      var url = "{{ route('dashboard.message.delete', ['id' => '']) . '/' }}" + id;
      
      $('#deleteMessageForm').prop('action', url);
    });

    $('#deleteImageModal').on('show.bs.modal', function (e) {
      var id  = $(e.relatedTarget).data('id');
      var url = "{{ route('dashboard.message.image.delete', ['id' => 'id']) }}";
      var url = url.replace('/id/', '/' + id + '/');
      
      $('#deleteImageForm').prop('action', url);
    });

    @if (session('notif'))
      $(document).ready(function () {
        var notification = $(".notification");

        notification.delay(500).fadeTo("slow", 1);

        notification.delay(3000).fadeTo("slow", 0, function () {
          notification.alert('close');
        });
      });
    @endif

  </script>
@endsection