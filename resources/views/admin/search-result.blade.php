<form method="post" id="formResult">
  @csrf
  <table class="table table-bordered">
    <thead>
      <tr>
        <th><input type="checkbox" id="selectAll"></th>
        <th>ID</th>
        <th>Title</th>
        <th>Body</th>
        <th width="200">Image</th>
        <th>Date</th>
        <th width="50">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($messages as $message)
        @if ($message->trashed())
        <tr class="bg-gray-light">
          <td>&nbsp;</td>
        @else
        <tr>
          <td><input type="checkbox" name="id[]" value="{{ $message->id }}"></td>
        @endif
          
          <td>{{ $message->id }}</td>
          <td>{{ $message->title }}</td>
          <td>{!! nl2br(e($message->body)) !!}</td>
          
          @if ($message->image && !$message->trashed())
            <td>
              <img class="img-prev" src="{{ asset($message->thumbnail_image) }}">
              <button type="button" data-toggle="modal" data-target="#deleteImageModal" data-id="{{ $message->id }}" class="btn btn-danger ml-10 btn-img" rel="tooltip" title="Delete Image">
                <i class="fa fa-trash"></i>
              </button>
            </td>
          @else
            <td>-</td>
          @endif

          <td>
            {{ $message->created_at->format('d-m-Y') }}<br/>
            <span class="small">{{ $message->created_at->format('H:i:s') }}</span>
          </td>
          @if ($message->trashed())
            <td>
              <button type="submit" formaction="{{ route('dashboard.message.recover', ['id' => $message->id]) }}" class="btn btn-default" rel="tooltip" title="Recover"><i class="fa fa-repeat"></i></button>
            </td>
          @else
            <td>
              <button type="button" data-toggle="modal" data-target="#deleteMessageModal" data-id="{{ $message->id }}" class="btn btn-danger" rel="tooltip" title="Delete">
                <i class="fa fa-trash"></i>
              </button>
            </td>
          @endif
        </tr>
      @endforeach
    </tbody>
  </table>
</form>

<button type="button" id="deleteAll" class="btn btn-default mt-5" data-toggle="modal" data-target="#deleteSelectedModal" disabled>Delete Checked Items</button>