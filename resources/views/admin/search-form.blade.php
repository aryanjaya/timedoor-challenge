<div class="bordered-box mb-20">
  <form class="form" role="form">
    <table class="table table-no-border mb-0">
      <tbody>
        <tr>
          <td width="80"><b>Title</b></td>
          <td>
            <div class="form-group mb-0">
              <input type="text" class="form-control" name="title" value="{{ request('title') }}">
            </div>
          </td>
        </tr>
        <tr>
          <td><b>Body</b></td>
          <td>
            <div class="form-group mb-0">
              <input type="text" class="form-control" name="body" value="{{ request('body') }}">
            </div>
          </td>
        </tr>
      </tbody>
    </table>
    <table class="table table-search">
      <tbody>
        <tr>
          <td width="80"><b>Image</b></td>
          <td width="60">
            <label class="radio-inline">
              @if (request('image_filter') === 'with')
                <input type="radio" name="image_filter" value="with" checked> with
              @else
                <input type="radio" name="image_filter" value="with"> with
              @endif
            </label>
          </td>
          <td width="80">
            <label class="radio-inline">
              @if (request('image_filter') === 'without')
                <input type="radio" name="image_filter" value="without" checked> without
              @else
                <input type="radio" name="image_filter" value="without"> without
              @endif
            </label>
          </td>
          <td>
            <label class="radio-inline">
              @if (request('image_filter') === 'unspecified' || empty(request('image_filter')))
                <input type="radio" name="image_filter" value="unspecified" checked> unspecified
              @else
                <input type="radio" name="image_filter" value="unspecified"> unspecified
              @endif
            </label>
          </td>
        </tr>
        <tr>
          <td width="80"><b>Status</b></td>
          <td>
            <label class="radio-inline">
              @if (request('status') === 'on')
                <input type="radio" name="status" value="on" checked> on
              @else
                <input type="radio" name="status" value="on"> on
              @endif
            </label>
          </td>
          <td>
            <label class="radio-inline">
              @if (request('status') === 'delete')
                <input type="radio" name="status" value="delete" checked> delete
              @else
                <input type="radio" name="status" value="delete"> delete
              @endif
            </label>
          </td>
          <td>
            <label class="radio-inline">
              @if (request('status') === 'unspecified' || empty(request('status')))
                <input type="radio" name="status"  value="unspecified" checked> unspecified
              @else
                <input type="radio" name="status"  value="unspecified"> unspecified
              @endif
            </label>
          </td>
        </tr>
        <tr>
          <td>
            <button type="submit" class="btn btn-default mt-10">
              <i class="fa fa-search"></i> Search
            </button>
          </td>
        </tr>
      </tbody>
    </table>
  </form>
</div>