<section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="{{ $page === 'home' ? 'active': '' }}">
          <a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> 
            <span>Dashboard</span>
          </a>
        </li>
      </ul>
</section>