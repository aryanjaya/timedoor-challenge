<!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="/plugin/bootstrap/bootstrap.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/plugin/font-awesome/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/css/admin.css">
    <!-- TMDR Preset -->
    <link rel="stylesheet" href="/css/tmdrPreset.css">
    <!-- Custom css -->
    <link rel="stylesheet" href="/css/custom.css">
    <!-- Skin -->
    <link rel="stylesheet" href="/css/skin.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->