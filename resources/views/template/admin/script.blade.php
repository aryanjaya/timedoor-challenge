<!-- jQuery 3 -->
    <script src="/plugin/jquery/jquery.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="/plugin/jquery/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="/plugin/bootstrap/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/js/adminlte.min.js"></script>