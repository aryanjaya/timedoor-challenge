@extends('layouts.auth')

@section('title', 'Timedoor Challenge - Level 9 | Register')

@section('content')
  <div id="login">
    <div class="box login-box">
      <div class="login-box-head">
        <h1 class="mb-5">Register</h1>
        <p class="text-lgray">Please fill the information below...</p>
      </div>

      <form action="{{ route('register') }}" method="post">
        @csrf
        <div class="login-box-body">
          <div class="form-group">
            <input type="text" class="form-control" name="name" placeholder="Name" value="{{ old('name') }}" required>
            @error('name')
              <p class="small text-danger mt-5">{{ $message }}</p>
            @enderror
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="email" placeholder="E-mail" value="{{ old('email') }}" required>
            @error('email')
              <p class="small text-danger mt-5">{{ $message }}</p>
            @enderror
          </div>
          <div class="form-group">
            <input type="password" class="form-control" name="password" placeholder="Password" required>
            @error('password')
              <p class="small text-danger mt-5">{{ $message }}</p>
            @enderror
          </div>
          <div class="form-group">
            <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
          </div>
        </div>
        <div class="login-box-footer">
          <div class="text-right">
            <a href="login.php" class="btn btn-default">Back</a>
            <button type="submit" class="btn btn-primary">Confirm</button>
          </div>
        </div>
      </form>
    </div>
  </div>
@endsection