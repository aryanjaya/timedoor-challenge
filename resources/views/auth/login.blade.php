@extends('layouts.auth')

@section('title', 'Timedoor Challenge - Level 9 | Login')

@section('content')
  <div id="login">
    <div class="box login-box">
      @if (session('denied'))
      <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Access denied!</strong> Please check your email to verify your account.
      </div>
      @endif

      <div class="login-box-head">
        <h1 class="mb-5">Login</h1>
        <p class="text-lgray">Please login to continue...</p>
      </div>
      <form action="{{ route('login') }}" method="post">
        @csrf
        <div class="login-box-body">
          <div class="form-group">
            <input type="text" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}">
            @error('email')
              <p class="small text-danger mt-5">{{ $message }}</p>
            @enderror
          </div>
          <div class="form-group">
            <input type="password" class="form-control" name="password" placeholder="Password">
            @error('password')
              <p class="small text-danger mt-5">{{ $message }}</p>
            @enderror
          </div>
        </div>
        <div class="login-box-footer">
          <div class="text-right">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>
@endsection