@extends('layouts.auth')

@section('title', 'Timedoor Challenge - Level 9 | Register Success')

@section('content')
  <div id="login">
    <div class="box login-box text-center">
      <div class="login-box-head">
        <h1>Successfully Registered</h1>
      </div>
      <div class="login-box-body">
        @if (session('resent'))
          <div class="alert alert-success" role="alert">
            {{ __('A fresh verification link has been sent to your email address.') }}
          </div>
        @endif

        <p>Thank you for your membership register.<br/>
          We send confirmation e-mail to you. Please complete the registration by clicking the confirmation URL. <br><br>
          If you did not receive the email, <a href="{{ route('verification.resend') }}">click here to request another'</a>.</p>
      </div>
      <div class="login-box-footer">
        <div class="text-center">
          <a href="{{ route('message.index') }}" class="btn btn-primary">Back to Home</a>
        </div>
      </div>
    </div>
  </div>
@endsection