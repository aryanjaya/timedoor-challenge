@extends('layouts.auth')

@section('title', 'Timedoor Challenge - Level 9 | Verification Expired')

@section('content')
  <div id="login">
    <div class="box login-box text-center">
      <div class="login-box-head">
        <h1>Verification Link Expired</h1>
      </div>
      <div class="login-box-body">
        @if (session('resent'))
          <div class="alert alert-success" role="alert">
            {{ __('A fresh verification link has been sent to your email address.') }}
          </div>
        @endif

        <p>Your verification link has been expired.<br/>
          You can resend verification link by click button below. <br><br>
          Please remember to click verification link before its expired.</p>
      </div>
      <div class="login-box-footer">
        <div class="text-center">
          <a href="{{ route('verification.resend') }}" class="btn btn-primary">Resend Verification Link</a>
        </div>
      </div>
    </div>
  </div>
@endsection