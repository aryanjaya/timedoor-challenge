<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('template.style')
    @yield('css')
  </head>

  <body class="bg-lgray">
    <header>
      @include('template.header')
    </header>
    <main>
      @yield('content')
    </main>
    
    <footer>
      @include('template.footer')
    </footer>

    @include('template.script')
    @yield('script')
  </body>
</html>