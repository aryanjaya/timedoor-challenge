<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::routes(['verify' => true]);
Auth::routes();
Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}', 'Auth\VerificationController@verifyUser')->name('verification.verify');
Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

Route::group(['as' => 'message.'], function () {
    Route::get('/', 'MessageController@index')->name('index');
    Route::post('/', 'MessageController@store')->name('store');
    
    Route::prefix('message')->group(function () {
        Route::post('edit', 'MessageController@edit')->name('edit');
        Route::post('delete', 'MessageController@delete')->name('delete');
        Route::put('{id}', 'MessageController@update')->name('update');
        Route::delete('{id}', 'MessageController@destroy')->name('confirm.delete');
    });
});

// Route List for Admin
Route::group([
    'prefix'     => 'admin',
    'namespace'  => 'Admin',
    'middleware' => 'admin'
], function () {
    Route::get('/', function () {
        return redirect()->route('dashboard.index');
    });

    Route::group(['prefix'=> 'dashboard', 'as' => 'dashboard.'], function () {
        Route::get('/', 'DashboardController@index')->name('index');
    
        Route::group(['prefix'=> 'message', 'as' => 'message.'], function () {
            Route::post('multi-delete', 'DashboardController@multiDelete')->name('multi-delete');
            Route::post('{id}/recover', 'DashboardController@recover')->name('recover');
            Route::delete('{id}', 'DashboardController@delete')->name('delete');
            Route::delete('{id}/image', 'DashboardController@deleteImage')->name('image.delete');
        });
    });
});